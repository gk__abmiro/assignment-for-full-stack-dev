# Assignment for Full-Stack Laravel+Angular

Create a private git repo on gitlab and implement the following project.

## Goal
### Coding
1. Create a Laravel 10 project
2. Use MySQL as DB (in Docker container)
3. Use Laravel Sanctum for authentication
4. Add models:
    - User (first_name, last_name, email, contact_number, created_at, updated_at)
    - Group (title, description, created_at, updated_at)
5. Add relatioships
    - Multiple users will be a part of a group.
    - A user can be part of multiple groups.
    - Make all models soft deletable
6. Flow
    - User will register
    - Will be a part of 2 groups
7. Create an angular app to handle the flow.
8. REST APIs to add, delete, modify single or multiple
    - User (s)
    - Group (s) 
9. REST APIs to bulk query Users or Groups over any combinations of the fields of the two models
10. Postman file demonstrating the REST APIs. See:
    - [https://apitransform.com/how-to-export-all-collections-from-postman/](https://apitransform.com/how-to-export-all-collections-from-postman/)
    - [https://github.com/postmanlabs/newman](https://github.com/postmanlabs/newman)
11. Tests
    1. Unit tests for Login, Register & Add Group
    2. No model access in test except for verification if not possible with only APIs
12. Lint your Laravel / Angular code
13. Code coverage (exclude all 3rd party libraries) (Optional)
14. Document code with Swagger (Optional)

### Deliverables (pushed to private repo)
_**include this README and check if completed or explain why it isn't done**_
- [ ] Source code
- [ ] Development setup instructions including commands for running lint and tests with coverage
<br>_example template_
> ### Development Setup
> ```
> 1. git clone <your repo url here>
> 2. docker run mysql ...
> 3. php artisan migrate
> ...
> 4. php-cs-fixer/pint ...
> 5. php artisan test --coverage
> ```
- [ ] Php-CS-fixer / Pint run report (expected no errors or warnings)
- [ ] Coverage report (Optional)
- [ ] Postman json



### Selection Criteria
1. Make sure that checklist above is complete
2. In case any point on the checklist is failed to be accomplished please provide all attempt information and reason for failure in details

## Code Evaluation
Provide `gk__abmiro`, `ganeshabmiro` and `ashokwagh` with **read** access to your repository.

## Interview Demo
You'll have to demonstrate & answer questions on the Assignment.
